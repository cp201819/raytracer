//
//  SimpleSensor.cpp
//  RayTracer
//
//  Created by Fernando Pedro Birra on 03/05/16.
//  Copyright © 2016 FCT/UNL. All rights reserved.
//

#include "SimpleSensor.h"

SimpleSensor::SimpleSensor() : ImageSensor()
{
};

SimpleSensor::SimpleSensor(int w, int h) : ImageSensor(w, h)
{
};

SimpleSensor::~SimpleSensor()
{
}

void SimpleSensor::process()
{
    // Do noting
}
